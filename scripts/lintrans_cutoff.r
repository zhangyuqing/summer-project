linearTransform <- function(x, quants = c(0.025, 0.975), to = c(-1, 
				1), applyto = "rows") {
	if (identical(class(x), "data.frame")) {
		warning("Coercing x to matrix")
		x <- as.matrix(x)
	}
	if (identical(class(x), "integer")) {
		warning("Coercing x to numeric")
		x <- numeric(x)
	}
	if (identical(class(x), "numeric")) {
		from <- quantile(x, quants)
		return(((x - from[1]) * diff(to)/diff(from)) + to[1])
	} else if (identical(class(x), "matrix")) {
		if (identical(applyto, "columns")) {
			x <- t(x)
		} else if (identical(applyto, "rows")) {
			froms <- t(apply(x, 1, quantile, probs = quants))
			fromdiffs <- froms[, 2] - froms[, 1]
			output <- sweep(sweep(x, 1, froms[, 1]) * diff(to), 
					1, fromdiffs, "/") + to[1]
			output[which(output <= to[1])] <- to[1]
			output[which(output >= to[2])] <- to[2]
		} else {
			stop("applyto should be: rows or columns")
		}
		if (identical(applyto, "columns")) 
			output <- t(output)
		return(output)
	} else {
		stop("Could not coerce x into matrix or numeric.")
	}
} 
