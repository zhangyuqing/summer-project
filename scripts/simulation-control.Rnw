\documentclass{article}

\begin{document}
\SweaveOpts{concordance=TRUE}

\section{Intro--Load Libraries and Functions}
<<echo=T,results=hide>>=
require(survival)  
require(pls)
require(CoxBoost)
require(Biobase)
require(survHD)
require(gbm)
require(Hmisc)
library(superpc)
library(gplots)
library(ClassDiscovery)
library(meta)
setwd("~/workspace/scripts/test6-updatefunctionsandimplementation")
load("ExpressionSetsList_rm_new.RData")
source("simData.R")
source("getTrueModel.R")
source("simTime.R")
source("simBootstrap.R")
source("plusminuscore.R")
source("masomenos.R")
source("cvSubsets.R")
source("funCV.R")
source("zmatrix.R")
source("geneFilter.R")
step <- 100
balance.variables <- c("size", "age", "node", "grade")
largeN <- 150
iterations <- 100
fold <- 4
set.seed(732)
y.vars <- list()
for(i in 1:length(esets)){  
  time <- esets[[i]]$dmfs.time
  time <- as.numeric(as.character(time))
  cens <- esets[[i]]$dmfs.cens
  cens <- as.numeric(as.character(cens))
  y.vars[[i]] <- Surv(time, cens)
}
@

\section{Simulation-Control}
<<boxplots-origin,echo=TRUE,fig=TRUE>>=
load("ExpressionSetsList_rm_new.RData")
y.vars <- list()
for(i in 1:length(esets)){  
  time <- esets[[i]]$dmfs.time
  time <- as.numeric(as.character(time))
  cens <- esets[[i]]$dmfs.cens
  cens <- as.numeric(as.character(cens))
  y.vars[[i]] <- Surv(time, cens)
}
Z.list <- list()
CV <- CSV <- c()

for(b in 1:iterations){
  print(paste("iteration: ", b, sep=""))
  sim2.esets <- simBootstrap(esets=esets, n.samples=largeN, y.vars=y.vars,
                             parstep=step, type="two-steps")
  Z.list[[b]] <- zmatrix(esets=sim2.esets$esets.list, 
                         y.vars=sim2.esets$y.vars.list, fold=fold,
                         trainingFun=plusMinus)
  sum.cv <- 0
  for(i in 1:length(esets)){
    sum.cv <- sum.cv + Z.list[[b]][i, i]
  }
  CV[b] <- sum.cv / length(esets)
  CSV[b] <- (sum(Z.list[[b]]) - sum.cv) / (length(esets)*(length(esets)-1))
}

resultlist <- list(CSV=CSV, CV=CV)
boxplot(resultlist, col=c("white", "grey"), ylab="C-Index", boxwex = 0.25, xlim=c(0.5, 2.5), ylim=c(0.5, 0.7))
@

\end{document}