\documentclass[a4paper,11pt,oneside]{article}
\usepackage[left=2.5cm,top=2cm,bottom=2cm,right=2.5cm]{geometry}
\usepackage{parskip}
\usepackage{url}
\usepackage{amstext}
\usepackage{tabularx}
\usepackage{booktabs,longtable,tabularx,graphicx}
\usepackage{amssymb, amsmath, enumerate}
\usepackage{amsmath}
\usepackage{dsfont}
\usepackage{Sweave}
\usepackage{hyperref}
\usepackage{rotating}
\usepackage{graphics}
\usepackage{graphicx}
\usepackage{pdfpages}
\usepackage{geometry}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{bm}
\bmdefine\bmu{\mu}
\bmdefine\bbeta{\beta}
\bmdefine\bSigma{\Sigma}
\newcommand{\xb}{\boldsymbol{x}}
\linespread{1.3}\normalsize
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 
\newcommand{\ra}{\rightarrow}
\newcommand{\Ra}{\Rightarrow}
\newcommand{\lra}{\Leftrightarrow}
\newcommand{\im}{\item}
\newcommand{\tr}{^\prime}
\newcommand{\bc}{\begin{center}}
\newcommand{\ec}{\end{center}}
\newcommand{\ben}{\begin{enumerate}}
\newcommand{\een}{\end{enumerate}}
\newcommand{\beq}{\begin{equation*}}
\newcommand{\eeq}{\end{equation*}}
\newcommand{\bea}{\begin{align*}}
\newcommand{\eea}{\end{align*}}
\newcommand{\bi}{\begin{itemize}}
\newcommand{\ei}{\end{itemize}}
\newcommand{\eps}{\varepsilon}
\newcommand{\sigmaeps} {\sigma^2_{\eps}}
\newcommand{\iid} {\operatorname{i.i.d.}}
\newcommand{\bvec}{\left[\begin{array}{c}}
\newcommand{\evec}{\end{array}\right]}
\newcommand{\bmat}[1]{\left[\begin{array}{*{#1}{c}}}
\newcommand{\emat}{\end{array}\right]}
\newcommand{\mean}{\operatorname{mean}}
\newcommand{\blockdiag}{\operatorname{blockdiag}}
\newcommand{\Rcmd}[1]{{\tt\color{lmugreen} #1}}
\DeclareMathOperator{\reell}{\mathds{R}}
\DeclareMathOperator{\nat}{\mathds{N}}
\DeclareMathOperator{\E}{E}
\DeclareMathOperator{\Var}{Var}  \DeclareMathOperator{\Cov}{Cov}
\DeclareMathOperator{\Corr}{Corr}
\DeclareMathOperator{\diag}{diag}

\title{\vspace{9cm}Validity checks of DGP}
\date{\today}
\begin{document}
\SweaveOpts{concordance=TRUE}

\section{Intro--Load Libraries and Functions}
<<echo=T,results=hide>>=
require(survival)  
require(pls)
require(CoxBoost)
require(Biobase)
require(survHD)
require(gbm)
require(Hmisc)
library(superpc)
library(gplots)
library(ClassDiscovery)
library(meta)
setwd("~/workspace/scripts/test6-updatefunctionsandimplementation")
load("ExpressionSetsList_rm_new.RData")
source("simData.R")
source("getTrueModel.R")
source("simTime.R")
source("simBootstrap.R")
source("plusminuscore.R")
source("masomenos.R")
source("cvSubsets.R")
source("funCV.R")
source("zmatrix.R")
source("geneFilter.R")
step <- 100
balance.variables <- c("size", "age", "node", "grade")
largeN <- 150
iterations <- 100
fold <- 4
set.seed(732)
y.vars <- list()
for(i in 1:length(esets)){  
  time <- esets[[i]]$dmfs.time
  time <- as.numeric(as.character(time))
  cens <- esets[[i]]$dmfs.cens
  cens <- as.numeric(as.character(cens))
  y.vars[[i]] <- Surv(time, cens)
}
@


\section{Heatmaps}
\subsection{Original}
<<heatmap-original,echo=TRUE,fig=TRUE>>=
Z.original <- zmatrix(esets=esets, y.vars=y.vars, fold=fold)

heat1 <- Z.original
pal<-blueyellow(15)[4:15]
rownames(heat1)<-c("CAL", "MNZ", "TAM1", "TAM2","TRB", "UNT", "VDX")
colnames(heat1)<-c("CAL", "MNZ", "TAM1", "TAM2","TRB", "UNT", "VDX")
heatmap.2(heat1,scale='none',dendrogram='none',Rowv=NA,Colv=NA,trace='none',col=pal,breaks=c(0,0.4,0.5,0.53,0.56,0.6,0.64,0.68,0.74,0.8,0.85,0.9,1),margins=c(5,5),lwid=c(1.7,3.8),lhei=c(1.7,3.8),keysize=5)
@

\subsection{Average Simulated}
<<heatmap-simulated,echo=TRUE,fig=TRUE>>=
Z.list <- list()
for(b in 1:iterations){
  sim1.esets <- simBootstrap(esets=esets, y.vars=y.vars, n.samples=largeN, 
                             parstep=step, type="one-step")
  Z.list[[b]] <- zmatrix(esets=sim1.esets$esets.list, 
                         y.vars=sim1.esets$y.vars.list, fold=fold)
}
sum.Z <- matrix(rep(0, length(esets)*length(esets)), nrow=length(esets))
count.itr <- 0
for(b in 1:iterations){
  marker <- TRUE
  for(i in 1:nrow(Z.list[[b]])){
    for(j in 1:ncol(Z.list[[b]])){
      if(is.nan(Z.list[[b]][i, j])) marker <- FALSE
    }
  }
  if(marker){
    sum.Z <- sum.Z + Z.list[[b]]
    count.itr <- count.itr + 1
  }    
}
Z.simulated <- sum.Z / count.itr
print(Z.simulated)

heat2 <- Z.simulated
pal<-blueyellow(15)[4:15]
rownames(heat2)<-c("CAL", "MNZ", "TAM1", "TAM2","TRB", "UNT", "VDX")
colnames(heat2)<-c("CAL", "MNZ", "TAM1", "TAM2","TRB", "UNT", "VDX")
heatmap.2(heat2,scale='none',dendrogram='none',Rowv=NA,Colv=NA,trace='none',col=pal,breaks=c(0,0.4,0.5,0.53,0.56,0.6,0.64,0.68,0.74,0.8,0.85,0.9,1),margins=c(5,5),lwid=c(1.7,3.8),lhei=c(1.7,3.8),keysize=5)
@

\section{Session Info}
<<echo=T>>=
sessionInfo()
@ 

\end{document}