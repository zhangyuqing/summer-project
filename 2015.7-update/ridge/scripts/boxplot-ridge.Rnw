\documentclass{article}

\begin{document}
\SweaveOpts{concordance=TRUE}
\section{Boxplots}

<<echo=TRUE,fig=TRUE>>=
numbox <- 6
setwd("~/workspace/validations")
#load("test_experimental.RData")
load("ridge_control.RData")
load("ridge_covariates.RData")
load("ridge_expressioncovariance.RData")
load("ridge_truemodel_2.RData")
load("ridge_difcoeff_2.RData")
load("ridge_alltogether_2.RData")

res <- list(result_1[[1]], result_1[[2]],
            result_2[[1]], result_2[[2]],
            result_3[[1]], result_3[[2]],
            result_4[[1]], result_4[[2]],
            result_5[[1]], result_5[[2]],
            result_6[[1]], result_6[[2]])
names(res) <- NULL
color.seq <- rep(c("white","grey"), times=6)
par(xpd=T,mar=par()$mar+c(2,1,0,9))
## +c(2,1,0,9) for 6 scenarios
## +c() for 4 scenarios
boxplot(res, col=color.seq,ylab="C-Index",xlab="Simulations (ridge regression)",boxwex = 0.6, width=rep(0.1,2*numbox), cex.axis=1.2, cex.lab=1.2)
#legend(13.1, 0.65, legend=c("CSV","CV"),col=c("black","black"),pch=c(0,21), cex=1.2,text.width=0.5, box.col="white")
points(13.4, 0.62, pch=0)
points(13.4, 0.59, pch=22, bg="grey")
text(x=14.2,y=0.62,labels=c("CSV"),cex=1.2)
text(x=14.0,y=0.59,labels=c("CV"),cex=1.2)
## 13.1 for 6 scenarios
## 9.1 for 4 scenarios
lines(rep(2.5,116),seq(0.5,0.73,0.002),type="l",lty=2)
lines(rep(4.5,116),seq(0.5,0.73,0.002),type="l",lty=2)
lines(rep(6.5,116),seq(0.5,0.73,0.002),type="l",lty=2)
lines(rep(8.5,116),seq(0.5,0.73,0.002),type="l",lty=2)
lines(rep(10.5,116),seq(0.5,0.73,0.002),type="l",lty=2)

text(x=1.5,y=0.78,labels=c("(a)"),cex=1.2)
text(x=3.5,y=0.78,labels=c("(b)"),cex=1.2)
text(x=5.5,y=0.78,labels=c("(c)"),cex=1.2)
text(x=7.5,y=0.78,labels=c("(d)"),cex=1.2)
text(x=9.5,y=0.78,labels=c("(e)"),cex=1.2)
text(x=11.5,y=0.78,labels=c("(f)"),cex=1.2)
par(mar=c(5, 4, 4, 2) + 0.1)

@

\end{document}
