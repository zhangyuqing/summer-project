\documentclass{article}

\begin{document}
\SweaveOpts{concordance=TRUE}

\section{Intro--Load Libraries and Functions}
<<echo=T,results=hide>>=
library(simulatorZ)
library(gbm)
library(glmnet)
setwd("~/workspace/Validations")
source("getTrueModel_update.R")
source("changevar.R")
load("ExpressionSetsList_rm_new.RData")
step <- 100
largeN <- 150
iterations <- 100
balance.variables <- c("size", "age", "grade")
fold <- 4
set.seed(732)
y.vars <- list()
for(i in 1:length(esets)){  
  time <- esets[[i]]$dmfs.time
  time <- as.numeric(as.character(time))
  cens <- esets[[i]]$dmfs.cens
  cens <- as.numeric(as.character(cens))
  y.vars[[i]] <- Surv(time, cens)
}
pfactor <- 1.8

source("C:/Users/toshiba/Documents/workspace/scripts/test9-improvement/test9/zmatrix_ridge.R")
source("C:/Users/toshiba/Documents/workspace/scripts/test9-improvement/test9/funCV_ridge.R")
@

\section{Using the same true model}
<<boxplots-same-model,echo=TRUE,fig=TRUE>>=
Z.list <- list()
CV <- CSV <- c()
library(Hmisc)

## construct whole ExpressionSet
  X.list <- lapply(esets, function(eset){
    newx <- t(exprs(eset))
    return(newx)
  }) 
  all.X <- do.call(rbind, X.list)
  cov.list <- lapply(esets, function(eset){
    return(pData(eset)[, balance.variables])
  })  
  all.cov <- as(data.frame(do.call(rbind, cov.list)), "AnnotatedDataFrame")
  all.yvars <- do.call(rbind, y.vars)
  whole_eset <- ExpressionSet(t(all.X), all.cov)
  
  lp.index <- c()
  for(i in 1:length(esets)){
    lp.index <- c(lp.index, rep(i, length(y.vars[[i]][, 1])))
  }
  
  ## get true model
  truemod <- getTrueModel(list(whole_eset), list(all.yvars), 
                          parstep=step, balance.variables)

for(b in 1:iterations){
  print(paste("iteration: ", b, sep=""))
  
  
  simmodels <- simData(esets, 150, y.vars)
  beta <- grid <- survH <- censH <- lp <- list()
  for(listid in 1:length(esets)){
    beta[[listid]] <- truemod$beta[[1]] * pfactor
    grid[[listid]] <- truemod$grid[[1]]
    survH[[listid]] <- truemod$survH[[1]]
    censH[[listid]] <- truemod$censH[[1]]
    lp[[listid]] <- (truemod$lp[[1]]*pfactor)[which(lp.index==listid)]
  }
  res <- list(beta=beta, grid=grid, survH=survH, censH=censH, lp=lp)
  simmodels <- simTime(simmodels, res)
  
  ## generate z matrix
  Z.list[[b]] <- zmatrix_ridge(obj=simmodels$obj, 
                         y.vars=simmodels$y.vars, fold=fold)
  sum.cv <- 0
  for(i in 1:length(esets)){
    sum.cv <- sum.cv + Z.list[[b]][i, i]
  }
  CV[b] <- sum.cv / length(esets)
  CSV[b] <- (sum(Z.list[[b]]) - sum.cv) / (length(esets)*(length(esets)-1))
}

resultlist <- list(CSV=CSV, CV=CV)
boxplot(resultlist, col=c("white", "grey"), ylab="C-Index", boxwex = 0.25, xlim=c(0.5, 2.5), ylim=c(0.5, 0.7))

result_4 <- resultlist
save(result_4, Z.list, file="ridge_truemodel_2.RData")
@

\end{document}