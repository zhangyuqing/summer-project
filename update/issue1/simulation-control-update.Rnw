\documentclass{article}

\begin{document}
\SweaveOpts{concordance=TRUE}

\section{Intro--Load Libraries and Functions}
<<echo=T,results=hide>>=
library(simulatorZ)
library(gbm)
setwd("~/workspace/validations")
source("getTrueModel_update.R")
source("changevar.R")
load("ExpressionSetsList_rm_new.RData")
step <- 100
balance.variables <- c("size", "age", "grade")
largeN <- 150
iterations <- 100
fold <- 4
set.seed(732)
y.vars <- list()
for(i in 1:length(esets)){  
  time <- esets[[i]]$dmfs.time
  time <- as.numeric(as.character(time))
  cens <- esets[[i]]$dmfs.cens
  cens <- as.numeric(as.character(cens))
  y.vars[[i]] <- Surv(time, cens)
}
@

\section{balancing covariates}
<<boxplots-balancing-covariates-update,echo=TRUE,fig=TRUE>>=
Z.list <- list()
CV <- CSV <- c()
truemod <- getTrueModel(esets, y.vars, 100, balance.variables)
for(b in 1:iterations){
  print(paste("iteration: ", b, sep=""))  
  simmodels <- simData(obj=esets, balance.variables=NULL,
                       n.samples=largeN, type="two-steps", y.vars=y.vars)
  sim3.esets <- simTime(simmodels=simmodels, result=truemod) 
  
  Z.list[[b]] <- zmatrix(obj=sim3.esets$obj, 
                         y.vars=sim3.esets$y.vars, fold=fold,
                         trainingFun=plusMinus)
  sum.cv <- 0
  for(i in 1:length(esets)){
    sum.cv <- sum.cv + Z.list[[b]][i, i]
  }
  CV[b] <- sum.cv / length(esets)
  CSV[b] <- (sum(Z.list[[b]]) - sum.cv) / (length(esets)*(length(esets)-1))
}

resultlist <- list(CSV=CSV, CV=CV)
boxplot(resultlist, col=c("white", "grey"), ylab="C-Index", boxwex = 0.25, xlim=c(0.5, 2.5), ylim=c(0.5, 0.7))

result_2 <- resultlist
save(result_2, file="test_control.RData")
@

\end{document}